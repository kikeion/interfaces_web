Crea un sitio web con Wordpress (temática que desees). Puedes tomar como referencia alguna web realizada con Wordpress. 

Requisitos a cumplir:

Cambia la plantilla por defecto. --> ok
Estructura:
Debe tener una página Inicio donde presentes la organización (con imágenes y/o vídeo). Incluye un logotipo de tu creación. --> ok
Debe contener una página Quienes somos. --> ok
Debes contener un menú Servicios que informe sobre los servicios ofrecidos por la empresa. --> no
Debe contener una página con Aviso legal. --> ok
Debe contener una página Blog con al menos 4 entradas. --> ok
Debe contener un formulario de Contacto. --> ok
Debe incorporar acceso a redes sociales (Facebook, Youtube, Twitter,...). --> ok
Debe tener un pie de página que muestre los datos de la empresa (nombre, teléfono y email) y un mapa de Google con la localización.
Añade algunos plugins que consideres interesantes.
Crea al menos un usuario de cada rol de los posibles (Administrador, Editor, Autor, Colaborador y Sucriptor). Comprueba qué diferencia hay entre cada uno.
Documentación a entregar
Elabora un documento (PDF) con capturas del sitio en el que documentes tu trabajo:

Plantilla elegida
Estructura del sitio
Plugins instalados
Roles de usuario utilizados
Aportaciones que has hecho a los requerimientos iniciales
...